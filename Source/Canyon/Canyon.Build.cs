// Copyright Martin Jablonsky. All Rights Reserved.

using UnrealBuildTool;

public class Canyon : ModuleRules
{
    public Canyon(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PrivateDependencyModuleNames.AddRange(new string[] { "Core" });

        PrivateIncludePaths.AddRange(
            new string[] {
                    "Canyon/Private",
            }
        );
    }
}