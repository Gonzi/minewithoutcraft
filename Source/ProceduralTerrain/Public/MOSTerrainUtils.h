// Copyright 2020 Martin Jablonsky, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "MOSTerrainUtils.generated.h"

UENUM()
enum class EEditMode : uint8
{
    Delete,
    Add
};

UENUM()
enum class EBlockType : uint8
{
    Air UMETA(DisplayName = "Air"),
    Lava UMETA(DisplayName = "Lava"),
    Sand UMETA(DisplayName = "Sand"),
    Ground UMETA(DisplayName = "Ground"),
    Rock UMETA(DisplayName = "Rock"),
    Snow UMETA(DisplayName = "Snow"),
};

USTRUCT()
struct FBlockDefinition
{
    GENERATED_BODY()

        UPROPERTY(config, EditAnywhere, Category = Game)
        int BlockHealth;

    UPROPERTY(config, EditAnywhere, Category = Game)
        uint8 AtlasIndex;

    UPROPERTY(config, EditAnywhere, Category = Game)
        uint8 AtlasIndexTop;
};