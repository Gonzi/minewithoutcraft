// Copyright 2020 Martin Jablonsky, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "Templates/Tuple.h"
#include "MOSTerrainUtils.h"
#include "PTSubsystem.generated.h"

UCLASS()
class PROCEDURALTERRAIN_API UPTSubsystem : public UGameInstanceSubsystem
{
    GENERATED_BODY()

public:
    float BlockSize = 100.f;
    FIntVector BlocksInChunk = FIntVector(32, 32, 128);
    uint8 MaxHeight = 96;
    uint8 DrawRadius = 6;

    UPTSubsystem();

    void AddBlock(const FVector& Location, EBlockType Type);
    void DamageBlock(const FVector& Location, int Force);
    FVector BlockLocation(const FVector& Location);
    void SaveState();
    void LoadState();

    void AddBlock(const FIntVector& BlockKey, EBlockType Type, bool UpdateMesh = true);
    EBlockType BlockAt(const FIntVector& BlockKey);
    int32 HealthAt(const FIntVector BlockKey);
    FBlockDefinition BlockTypeInfo(const EBlockType Type);

    int32 BlockKeyToIndex(const FIntVector& BlockKey);
    FIntVector ChunkKeyToBlockKey(const FIntVector& ChunkKey);
    FIntVector LocationToBlockKey(const FVector& Location);
    FIntVector LocationToChunkKey(const FVector& Location);
    FVector ChunkKeyToLocation(const FIntVector& ChunkKey);
    FIntVector BlockKeyToChunkKey(const FIntVector& BlockKey);

    TArray<EBlockType> GetChunkData(const FIntVector& ChunkFrom);
    EBlockType NoiseAt(const FIntVector& BlockKey);

protected:
    FVector ReferencePosition = FVector(0, 0, 0);
    class FDelegateHandle TickDelegateHandle;

    TMap<FIntVector, TMap<FIntVector, EBlockType>> UserChunks;
    TMap<FIntVector, int32> EditedHealth;
    TMap<FIntVector, class AMOSTerrainChunk*> TerrainChunks;
    TMap<EBlockType, FBlockDefinition> BlockDefinition;

    bool Tick(float DeltaSeconds);
    virtual void Initialize(class FSubsystemCollectionBase& Collection) override;
    virtual void Deinitialize() override;

    bool ChunkExists(const FIntVector& ChunkKey);
    void UpdateReferencePosition();
    void AddMissingChunks();
    void RemoveDistantChunks();
    bool InRange(const FIntVector& ChunkKey);
    void SpawnChunk(const FIntVector& ChunkKey);
    void DespawnChunk(const FIntVector& ChunkKey);
};