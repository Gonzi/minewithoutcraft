// Copyright 2020 Martin Jablonsky, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MOSTerrainUtils.h"

#include "MOSTerrainChunk.generated.h"

UCLASS()
class PROCEDURALTERRAIN_API AMOSTerrainChunk : public AActor
{
    GENERATED_BODY()

public:
    AMOSTerrainChunk();
    void Init(FIntVector ChunkKey, const FIntVector& BlocksInChunk, float BlockSize);
    void ChangeBlock(const FIntVector& BlockKey, EBlockType Type);

protected:
    struct FIntVector ChunkKey;
    TArray<EBlockType> ChunkBlocks;
    class UMOSChunkMesh* ChunkMesh;
};