// Copyright 2020 Martin Jablonsky, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MOSTerrainUtils.h"

#include "MOSTerrainManipulatorComponent.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROCEDURALTERRAIN_API UMOSTerrainManipulatorComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    UMOSTerrainManipulatorComponent();

    UPROPERTY(EditAnywhere, Category = "Terrain")
        EEditMode EditMode = EEditMode::Delete;

    UPROPERTY(EditAnywhere, Category = "Terrain")
        float InteractionRange = 1000.f;

    UPROPERTY(EditAnywhere, Category = "Terrain")
        float InteractionCloseRange = 100.f;

    UPROPERTY(EditAnywhere, Category = "Terrain")
        EBlockType ActiveBlockType;

    UPROPERTY(EditAnywhere, Category = "Terrain")
        int DamageOutput = 10;

    UPROPERTY(EditAnywhere, Category = "Terrain")
        class UMaterialInstanceDynamic* PreviewMaterial;

    UPROPERTY(BlueprintReadOnly, Category = "Terrain")
        bool IsValidLookingAt = false;

    UPROPERTY(BlueprintReadOnly, Category = "Terrain")
        FVector LookingAtLocation;

    UFUNCTION(BlueprintCallable, Category = "Terrain")
        void QuickSave();

    UFUNCTION(BlueprintCallable, Category = "Terrain")
        void QuickLoad();

    UFUNCTION(BlueprintCallable, Category = "Terrain")
        void Interact();

    UFUNCTION(BlueprintCallable, Category = "Terrain")
        void ChangeMode();

protected:
    class UPTSubsystem* Terrain;
    FName AtlasIndexParamName;
    virtual void BeginPlay() override;
    void InitPreviewCube();
    void UpdatePreviewCube();
    bool LineTrace(struct FHitResult& OutHit, bool WithSafety = true);
    class UStaticMeshComponent* PreviewCube;
    TArray<EBlockType> UserPlacableBlocks;
    int ActiveBlockIndex;

public:
    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
