// Copyright 2020 Martin Jablonsky, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "MOSTerrainUtils.h"

#include "MOSSettings.generated.h"

#define MOS_TERRAIN_CHANNEL ECollisionChannel::ECC_GameTraceChannel1

/**
 * based on https://nerivec.github.io/old-ue4-wiki/pages/customsettings.html
 */
UCLASS(config = Game, defaultconfig, meta = (DisplayName = "Mine of Steel"))
class UMOSSettings : public UDeveloperSettings
{
    GENERATED_BODY()

public:

    UPROPERTY(EditAnywhere, config, Category = Game, meta = (UIMin = 50.f, ClampMin = 50.f, UIMax = 200.f, ClampMax = 200.f))
        float BlockSize = 100.f;

    UPROPERTY(EditAnywhere, config, Category = Game, meta = (UIMin = 2, ClampMin = 2, UIMax = 128, ClampMax = 128))
        uint8 BlocksInChunkWidth = 32;

    UPROPERTY(EditAnywhere, config, Category = Game, meta = (UIMin = 2, ClampMin = 2, UIMax = 128, ClampMax = 128))
        uint8 BlocksInChunkHeight = 96;

    UPROPERTY(EditAnywhere, config, Category = Game, meta = (UIMin = 2, ClampMin = 2, UIMax = 128, ClampMax = 128))
        uint8 TerrainMaxHeight = 96;

    UPROPERTY(EditAnywhere, config, Category = Game, meta = (UIMin = 1, ClampMin = 1, UIMax = 20, ClampMax = 20))
        uint8 ChunkDrawRadius = 6;

    UPROPERTY(config, EditAnywhere, Category = Game, meta = (AllowedClasses = "MaterialInterface"))
        FStringAssetReference TerrainMaterial;

    UPROPERTY(config, EditAnywhere, Category = Game, meta = (UIMin = 4, ClampMin = 4, UIMax = 16, ClampMax = 16))
        uint8 TerrainMaterialAtlasIndexSize;

    UPROPERTY(config, EditAnywhere, Category = Game, meta = (AllowedClasses = "StaticMesh"))
        FStringAssetReference TerrainEditCube;

    UPROPERTY(config, EditAnywhere, Category = Game, meta = (AllowedClasses = "MaterialInterface"))
        FStringAssetReference TerrainEditCubeMaterial;

    UPROPERTY(config, EditAnywhere, Category = Game)
        FName TerrainEditCubeMaterialAtlasIndexParamName = TEXT("AtlasIndex");

    UPROPERTY(config, EditAnywhere, Category = Game)
        TMap<EBlockType, FBlockDefinition> BlockSettings;

    UPROPERTY(config, EditAnywhere, Category = Game)
        TArray<EBlockType> UserPlacableBlocks;
};