// Copyright Martin Jablonsky. All Rights Reserved.

using UnrealBuildTool;

public class ProceduralTerrain : ModuleRules
{
    public ProceduralTerrain(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "ProceduralMeshComponent", "DeveloperSettings" });

        PrivateIncludePaths.AddRange(
            new string[] {
                    "ProceduralTerrain/Private",
            }
        );
        PublicIncludePaths.Add("ProceduralTerrain/Public");
    }
}