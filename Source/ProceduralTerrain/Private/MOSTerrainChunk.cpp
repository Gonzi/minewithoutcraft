// Copyright 2020 Martin Jablonsky, All Rights Reserved.

#include "MOSTerrainChunk.h"
#include "PTSubsystem.h"
#include "Math/UnrealMathUtility.h"
#include "Engine/Engine.h"
#include "Materials/Material.h"
#include "ProceduralMeshComponent.h"
#include "MOSChunkMesh.h"
#include "MOSTerrainUtils.h"
#include "UObject/Object.h"
#include "Math/IntVector.h"

AMOSTerrainChunk::AMOSTerrainChunk()
{
    ChunkMesh = CreateDefaultSubobject<UMOSChunkMesh>(TEXT("ChunkMesh"));
    SetRootComponent(ChunkMesh);
}

void AMOSTerrainChunk::Init(FIntVector chunkKey, const FIntVector& blocksInChunk, float blockSize)
{
    ChunkKey = FIntVector(chunkKey);
    UPTSubsystem* Terrain = GetGameInstance()->GetSubsystem<UPTSubsystem>();
    ChunkBlocks = Terrain->GetChunkData(ChunkKey);
    ChunkMesh->MakeMesh(ChunkBlocks);
}

void AMOSTerrainChunk::ChangeBlock(const FIntVector& BlockKey, EBlockType Type)
{
    UPTSubsystem* Terrain = GetGameInstance()->GetSubsystem<UPTSubsystem>();
    int32 BlockIndex = Terrain->BlockKeyToIndex(BlockKey);
    if (ChunkBlocks[BlockIndex] != Type)
    {
        ChunkBlocks[BlockIndex] = Type;
        ChunkMesh->MakeMesh(ChunkBlocks);
    }
}