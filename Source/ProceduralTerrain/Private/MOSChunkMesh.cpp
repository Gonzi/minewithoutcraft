// Copyright 2020 Martin Jablonsky, Inc. All Rights Reserved.

#include "MOSChunkMesh.h"
#include "PTSubsystem.h"
#include "ProceduralMeshComponent.h"
#include "Math/IntVector.h"
#include "MOSSettings.h"
#include "UObject/SoftObjectPath.h"

UMOSChunkMesh::UMOSChunkMesh()
{
    PrimaryComponentTick.bCanEverTick = true;
    CustomMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("CustomMesh"));
    CustomMesh->AttachToComponent(this, FAttachmentTransformRules::KeepRelativeTransform);
    CustomMesh->bUseAsyncCooking = true;
}

void UMOSChunkMesh::BeginPlay()
{
    Super::BeginPlay();
    Terrain = GetOwner()->GetGameInstance()->GetSubsystem<UPTSubsystem>();
    const UMOSSettings* Settings = GetDefault<UMOSSettings>();
    TerrainMaterial = Settings->TerrainMaterial;
    AtlasSideSize = Settings->TerrainMaterialAtlasIndexSize;
}

void UMOSChunkMesh::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UMOSChunkMesh::MakeMesh(const TArray<EBlockType>& ProceduralChunk)
{
    Triangles.Empty();
    Vertices.Empty();
    UVs.Empty();

    float BlockSize = Terrain->BlockSize;
    FIntVector BlocksInChunk = Terrain->BlocksInChunk;

    for (int32 x = 0; x < BlocksInChunk.X; x++)
    {
        for (int32 y = 0; y < BlocksInChunk.Y; y++)
        {
            for (int32 z = 0; z < BlocksInChunk.Z; z++)
            {
                int32 Key = x + y * BlocksInChunk.X + z * BlocksInChunk.X * BlocksInChunk.Y;
                int32 KeyFwd = x < BlocksInChunk.X - 1 ? Key + 1 : Key;
                int32 KeyRight = y < BlocksInChunk.Y - 1 ? Key + BlocksInChunk.X : Key;
                int32 KeyUp = z < BlocksInChunk.Z - 1 ? Key + BlocksInChunk.X * BlocksInChunk.Y : Key;

                EBlockType Block = ProceduralChunk[Key];
                EBlockType BlockFwd = ProceduralChunk[KeyFwd];
                EBlockType BlockRight = ProceduralChunk[KeyRight];
                EBlockType BlockUp = ProceduralChunk[KeyUp];

                FVector A = FVector(x, y, z) * BlockSize;
                FVector B = FVector(x, y + 1, z) * BlockSize;
                FVector C = FVector(x + 1, y + 1, z) * BlockSize;
                FVector D = FVector(x + 1, y, z) * BlockSize;

                FVector J = FVector(x, y, z + 1) * BlockSize;
                FVector K = FVector(x, y + 1, z + 1) * BlockSize;
                FVector L = FVector(x + 1, y + 1, z + 1) * BlockSize;
                FVector U = FVector(x + 1, y, z + 1) * BlockSize;

                if (Block != EBlockType::Air)
                {
                    if (KeyFwd == Key || BlockFwd == EBlockType::Air)
                    {
                        AddQuad(C, D, U, L, Block);
                    }
                    if (KeyRight == Key || BlockRight == EBlockType::Air)
                    {
                        AddQuad(B, C, L, K, Block);
                    }
                    if (KeyUp == Key || BlockUp == EBlockType::Air)
                    {
                        AddQuad(J, K, L, U, Block, true);
                    }
                }

                // fill last air (air ends, ground begins, so oposite draw order!
                if (Block == EBlockType::Air)
                {
                    if (BlockFwd != EBlockType::Air)
                    {
                        AddQuad(D, C, L, U, BlockFwd);
                    }
                    if (BlockRight != EBlockType::Air)
                    {
                        AddQuad(C, B, K, L, BlockRight);
                    }
                    if (BlockUp != EBlockType::Air)
                    {
                        AddQuad(U, L, K, J, BlockUp);
                    }
                }

                // fill start (chunk border will be filled even if the terrain will just continue in the next chunk
                if (x == 0 && Block != EBlockType::Air)
                {
                    AddQuad(A, B, K, J, Block);
                }

                if (y == 0 && Block != EBlockType::Air)
                {
                    AddQuad(D, A, J, U, Block);
                }
                if (z == 0 && Block != EBlockType::Air)
                {
                    AddQuad(D, C, B, A, Block);

                }
            }
        }
    }

    CustomMesh->CreateMeshSection_LinearColor(0, Vertices, Triangles, TArray<FVector>(), UVs, TArray<FLinearColor>(), TArray<FProcMeshTangent>(), true);
    UMaterial* Material = Cast<UMaterial>(StaticLoadObject(UMaterial::StaticClass(), nullptr, *TerrainMaterial.ToString(), nullptr, LOAD_None, nullptr));
    CustomMesh->SetMaterial(0, Material);
    CustomMesh->SetCollisionResponseToChannel(MOS_TERRAIN_CHANNEL, ECR_Block);

    Triangles.Empty();
    Vertices.Empty();
    UVs.Empty();
}

void UMOSChunkMesh::AddQuad(FVector V1, FVector V2, FVector V3, FVector V4, EBlockType Type, bool Top)
{
    FBlockDefinition BlockDefinition = Terrain->BlockTypeInfo(Type);
    int renderType = Top ? BlockDefinition.AtlasIndexTop : BlockDefinition.AtlasIndex;
    int count = Vertices.Num();
    float atlasStep = 1.0f / (float)AtlasSideSize;
    float Ustart = atlasStep * (renderType % AtlasSideSize);
    float Uend = Ustart + atlasStep;
    float Vstart = atlasStep * (renderType / AtlasSideSize);
    float Vend = Vstart + atlasStep;

    Vertices.Add(V1);
    Vertices.Add(V2);
    Vertices.Add(V3);
    Vertices.Add(V4);

    UVs.Add(FVector2D(Ustart, Vend));
    UVs.Add(FVector2D(Uend, Vend));
    UVs.Add(FVector2D(Uend, Vstart));
    UVs.Add(FVector2D(Ustart, Vstart));

    Triangles.Add(count);
    Triangles.Add(count + 1);
    Triangles.Add(count + 2);

    Triangles.Add(count);
    Triangles.Add(count + 2);
    Triangles.Add(count + 3);
}
