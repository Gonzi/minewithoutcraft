// Copyright 2020 Martin Jablonsky, Inc. All Rights Reserved.

#include "MOSTerrainManipulatorComponent.h"
#include "GameFramework/Pawn.h"
#include "Engine/Engine.h"
#include "MOSTerrainChunk.h"
#include "PTSubsystem.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/UObjectGlobals.h"
#include "Engine/EngineTypes.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "MOSSettings.h"
#include "ProceduralTerrain.h"
#include "CoreMinimal.h"

UMOSTerrainManipulatorComponent::UMOSTerrainManipulatorComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
}

void UMOSTerrainManipulatorComponent::BeginPlay()
{
    Super::BeginPlay();
    Terrain = GetOwner()->GetGameInstance()->GetSubsystem<UPTSubsystem>();
    const UMOSSettings* MOSSettings = GetDefault<UMOSSettings>();
    UserPlacableBlocks.Add(EBlockType::Air);
    ActiveBlockIndex = 0;
    ActiveBlockType = UserPlacableBlocks[ActiveBlockIndex];
    UserPlacableBlocks.Append(MOSSettings->UserPlacableBlocks.FilterByPredicate([](EBlockType type) { return type != EBlockType::Air; }));
    InitPreviewCube();
}

void UMOSTerrainManipulatorComponent::InitPreviewCube()
{
    const UMOSSettings* MOSSettings = GetDefault<UMOSSettings>();
    AtlasIndexParamName = MOSSettings->TerrainEditCubeMaterialAtlasIndexParamName;
    PreviewCube = NewObject<UStaticMeshComponent>(GetOwner(), UStaticMeshComponent::StaticClass());
    if (PreviewCube)
    {
        PreviewCube->RegisterComponent();
        UStaticMesh* Mesh = LoadObject<UStaticMesh>(nullptr, *MOSSettings->TerrainEditCube.ToString());
        if (Mesh)
        {
            PreviewCube->SetStaticMesh(Mesh);
            PreviewCube->SetCollisionResponseToChannel(MOS_TERRAIN_CHANNEL, ECR_Ignore);
        }
        UMaterial* Material = Cast<UMaterial>(StaticLoadObject(UMaterial::StaticClass(), nullptr, *MOSSettings->TerrainEditCubeMaterial.ToString(), nullptr, LOAD_None, nullptr));
        if (Material)
        {
            PreviewMaterial = UMaterialInstanceDynamic::Create(Material, this);
            PreviewMaterial->SetScalarParameterValue(AtlasIndexParamName, (float)ActiveBlockType);
            PreviewCube->SetMaterial(0, PreviewMaterial);
        }
    }
}

bool UMOSTerrainManipulatorComponent::LineTrace(FHitResult& OutHit, bool WithSafety)
{
    APawn* Owner = Cast<APawn>(GetOwner());
    FRotator EyeRotation = Owner->GetViewRotation();
    FVector EyeLocation = Owner->GetPawnViewLocation();
    FVector LookDirection = EyeRotation.RotateVector(FVector::ForwardVector);
    FVector TraceTooCloseEnd = ((LookDirection * InteractionCloseRange) + EyeLocation);
    FVector TraceEnd = ((LookDirection * InteractionRange) + EyeLocation);

    if (!WithSafety || !GetWorld()->LineTraceSingleByChannel(OutHit, EyeLocation, TraceTooCloseEnd, MOS_TERRAIN_CHANNEL, FCollisionQueryParams()))
    {
        if (GetWorld()->LineTraceSingleByChannel(OutHit, EyeLocation, TraceEnd, MOS_TERRAIN_CHANNEL, FCollisionQueryParams()))
        {
            return OutHit.bBlockingHit;
        }
    }
    return false;
}

void UMOSTerrainManipulatorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
    IsValidLookingAt = false;
    FHitResult OutHit;
    if (LineTrace(OutHit, EditMode == EEditMode::Add))
    {
        if (AMOSTerrainChunk* Chunk = Cast<AMOSTerrainChunk>(OutHit.GetActor()))
        {
            IsValidLookingAt = true;
            LookingAtLocation = EditMode == EEditMode::Add ? OutHit.ImpactPoint + OutHit.ImpactNormal : OutHit.ImpactPoint - OutHit.ImpactNormal;
        }
    }
    UpdatePreviewCube();
}

void UMOSTerrainManipulatorComponent::UpdatePreviewCube()
{
    return;
    if (IsValidLookingAt)
    {
        if (!PreviewCube->IsVisible())
        {
            PreviewCube->SetVisibility(true);
        }
        if (EditMode == EEditMode::Delete)
        {
            PreviewCube->SetWorldLocation(Terrain->BlockLocation(LookingAtLocation) - FVector(1, 1, 1));
            PreviewCube->SetWorldScale3D(FVector(1.02, 1.02, 1.02));
            PreviewMaterial->SetScalarParameterValue(AtlasIndexParamName, 14);
        }
        else
        {
            PreviewCube->SetWorldLocation(Terrain->BlockLocation(LookingAtLocation));
            PreviewCube->SetWorldScale3D(FVector(1, 1, 1));
            PreviewMaterial->SetScalarParameterValue(AtlasIndexParamName, Terrain->BlockTypeInfo(ActiveBlockType).AtlasIndex);
        }
    }
    else
    {
        if (PreviewCube->IsVisible())
        {
            PreviewCube->SetVisibility(false);
        }
    }
}

void UMOSTerrainManipulatorComponent::QuickSave()
{
    return Terrain->SaveState();
}

void UMOSTerrainManipulatorComponent::QuickLoad()
{
    return Terrain->LoadState();
}

void UMOSTerrainManipulatorComponent::Interact()
{
    if (!IsValidLookingAt)
    {
        return;
    }
    if (EditMode == EEditMode::Add)
    {
        Terrain->AddBlock(LookingAtLocation, ActiveBlockType);
    }
    else
    {
        Terrain->DamageBlock(LookingAtLocation, DamageOutput);
    }
}

void UMOSTerrainManipulatorComponent::ChangeMode()
{
    ActiveBlockIndex = (ActiveBlockIndex + 1) % (UserPlacableBlocks.Num() - 1);
    ActiveBlockType = UserPlacableBlocks[ActiveBlockIndex];
    EditMode = ActiveBlockType == EBlockType::Air ? EEditMode::Delete : EEditMode::Add;
}