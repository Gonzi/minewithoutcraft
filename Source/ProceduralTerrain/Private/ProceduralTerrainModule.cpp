// Copyright Martin Jablonsky. All Rights Reserved.

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"
#include "ProceduralTerrain.h"

DEFINE_LOG_CATEGORY(LogProceduralTerrain);

IMPLEMENT_MODULE(FDefaultModuleImpl, ProceduralTerrain);