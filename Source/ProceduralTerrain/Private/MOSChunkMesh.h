// Copyright 2020 Martin Jablonsky, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "MOSTerrainUtils.h"

#include "MOSChunkMesh.generated.h"

UCLASS()
class PROCEDURALTERRAIN_API UMOSChunkMesh : public USceneComponent
{
    GENERATED_BODY()

public:
    UMOSChunkMesh();
    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
    void MakeMesh(const TArray<EBlockType>& ProceduralChunk);

protected:
    uint8 AtlasSideSize;
    FStringAssetReference TerrainMaterial;
    class UPTSubsystem* Terrain;
    TArray<struct FVector> Vertices;
    TArray<int32> Triangles;
    TArray<struct FVector2D> UVs;
    class UProceduralMeshComponent* CustomMesh;
    void AddQuad(FVector V1, FVector V2, FVector V3, FVector V4, EBlockType Type, bool Top = false);
    virtual void BeginPlay() override;
};
