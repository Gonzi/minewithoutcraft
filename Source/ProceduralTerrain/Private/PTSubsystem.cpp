// Copyright 2020 Martin Jablonsky, All Rights Reserved.

#include "PTSubsystem.h"
#include "Engine/World.h"
#include "MOSTerrainChunk.h"
#include "Engine/GameInstance.h"
#include "Containers/Ticker.h"
#include "GameFramework/PlayerController.h"
#include "Containers/Map.h"
#include "Engine/Engine.h"
#include "Containers/UnrealString.h"
//#include "MOSSaveGame.h" // it should be elsewhere, this should just provide data for saving
#include "Kismet/GameplayStatics.h"
#include "Math/IntVector.h"
#include "MOSSettings.h"

UPTSubsystem::UPTSubsystem() {}

void UPTSubsystem::Initialize(class FSubsystemCollectionBase& Collection)
{
    Super::Initialize(Collection);
    const UMOSSettings* MOSSettings = GetDefault<UMOSSettings>();
    BlockSize = MOSSettings->BlockSize;
    BlocksInChunk = FIntVector(MOSSettings->BlocksInChunkWidth, MOSSettings->BlocksInChunkWidth, MOSSettings->BlocksInChunkHeight);
    MaxHeight = MOSSettings->TerrainMaxHeight;
    DrawRadius = MOSSettings->ChunkDrawRadius;
    BlockDefinition = MOSSettings->BlockSettings;
    TickDelegateHandle = FTicker::GetCoreTicker()
        .AddTicker(FTickerDelegate::CreateUObject(this, &UPTSubsystem::Tick));
}

void UPTSubsystem::Deinitialize()
{
    FTicker::GetCoreTicker().RemoveTicker(TickDelegateHandle);
    Super::Deinitialize();
}

bool UPTSubsystem::Tick(float DeltaSeconds)
{
    UpdateReferencePosition();
    AddMissingChunks();
    RemoveDistantChunks();
    return true;
}

FORCEINLINE bool UPTSubsystem::ChunkExists(const FIntVector& ChunkKey)
{
    return TerrainChunks.Contains(ChunkKey);
}

FORCEINLINE FIntVector UPTSubsystem::LocationToChunkKey(const FVector& Location)
{
    return FIntVector(
        FMath::FloorToInt(Location.X / (BlocksInChunk.X * BlockSize)),
        FMath::FloorToInt(Location.Y / (BlocksInChunk.Y * BlockSize)),
        0);
}

FORCEINLINE FIntVector UPTSubsystem::LocationToBlockKey(const FVector& Location)
{
    return FIntVector(
        FMath::FloorToInt(Location.X / BlockSize),
        FMath::FloorToInt(Location.Y / BlockSize),
        FMath::FloorToInt(Location.Z / BlockSize));
}

FORCEINLINE FIntVector UPTSubsystem::BlockKeyToChunkKey(const FIntVector& BlockKey)
{
    return FIntVector(
        FMath::FloorToInt((float)BlockKey.X / (float)BlocksInChunk.X),
        FMath::FloorToInt((float)BlockKey.Y / (float)BlocksInChunk.Y),
        0);
}

FIntVector UPTSubsystem::ChunkKeyToBlockKey(const FIntVector& ChunkKey)
{
    return FIntVector(
        ChunkKey.X * BlocksInChunk.X,
        ChunkKey.Y * BlocksInChunk.Y,
        0);
}

int32 UPTSubsystem::BlockKeyToIndex(const FIntVector& BlockKey)
{
    FIntVector ChunkKey = BlockKeyToChunkKey(BlockKey);
    ChunkKey.X *= BlocksInChunk.X;
    ChunkKey.Y *= BlocksInChunk.Y;
    ChunkKey.Z *= BlocksInChunk.Z;
    FIntVector Local = BlockKey - ChunkKey;
    return Local.X + Local.Y * BlocksInChunk.X + Local.Z * BlocksInChunk.X * BlocksInChunk.Y;
}

FORCEINLINE FVector UPTSubsystem::ChunkKeyToLocation(const FIntVector& ChunkKey)
{
    return FVector(
        ChunkKey.X * BlocksInChunk.X * BlockSize,
        ChunkKey.Y * BlocksInChunk.Y * BlockSize,
        ChunkKey.Z * BlocksInChunk.Z * BlockSize);
}

FVector UPTSubsystem::BlockLocation(const FVector& Location)
{
    return FVector(LocationToBlockKey(Location)) * BlockSize;
}

// it should be elsewhere, this should just provide data for saving
void UPTSubsystem::SaveState()
{
   /* if (UMOSSaveGame* SaveGameData = Cast<UMOSSaveGame>(UGameplayStatics::CreateSaveGameObject(UMOSSaveGame::StaticClass())))
    {
        for (auto Chunk = UserChunks.CreateConstIterator(); Chunk; ++Chunk)
        {
            for (auto Block = Chunk.Value().CreateConstIterator(); Block; ++Block)
            {
                SaveGameData->TerrainData.Emplace(Block.Key(), Block.Value());
            }
        }

        UGameplayStatics::SaveGameToSlot(SaveGameData, "DefaultSave", 0);
    }*/
}

// it should be elsewhere, this should just provide data for saving
void UPTSubsystem::LoadState()
{
    //if (UMOSSaveGame* LoadGameData = Cast<UMOSSaveGame>(UGameplayStatics::LoadGameFromSlot("DefaultSave", 0)))
    //{
    //    TMap<FIntVector, bool> TouchedChunks;
    //    // mark chunks with user data
    //    for (auto Chunk = UserChunks.CreateConstIterator(); Chunk; ++Chunk)
    //    {
    //        if (Chunk.Value().Num() > 0)
    //        {
    //            TouchedChunks.Emplace(Chunk.Key(), true);
    //        }
    //    }

    //    UserChunks.Empty();

    //    for (auto Block = LoadGameData->TerrainData.CreateConstIterator(); Block; ++Block)
    //    {
    //        AddBlock(Block.Key(), Block.Value(), false);
    //        // mark chunks with loaded data
    //        TouchedChunks.Emplace(BlockKeyToChunkKey(Block.Key()), true);
    //    }

    //    // reload changed chunks
    //    for (auto Chunk = TouchedChunks.CreateConstIterator(); Chunk; ++Chunk)
    //    {
    //        if (ChunkExists(Chunk.Key()))
    //        {
    //            TerrainChunks[Chunk.Key()]->Init(Chunk.Key(), BlocksInChunk, BlockSize);
    //        }
    //    }
    //}
}

EBlockType UPTSubsystem::BlockAt(const FIntVector& BlockKey)
{
    FIntVector ChunkKey = BlockKeyToChunkKey(BlockKey);
    if (UserChunks.Contains(ChunkKey) && UserChunks[ChunkKey].Contains(BlockKey))
    {
        return UserChunks[ChunkKey][BlockKey];
    }
    return NoiseAt(BlockKey);
}

int32 UPTSubsystem::HealthAt(const FIntVector BlockKey)
{
    if (EditedHealth.Contains(BlockKey))
    {
        return EditedHealth[BlockKey];
    }
    return BlockTypeInfo((EBlockType)BlockAt(BlockKey)).BlockHealth;
}

FBlockDefinition UPTSubsystem::BlockTypeInfo(const EBlockType Type)
{
    if (BlockDefinition.Contains(Type))
    {
        return BlockDefinition[Type];
    }
    return FBlockDefinition{ 10, 1, 1 };
}

void UPTSubsystem::DamageBlock(const FVector& Location, int32 Force)
{
    FIntVector BlockKey = LocationToBlockKey(Location);
    int32 Health = HealthAt(BlockKey);

    if (Force < Health)
    {
        EditedHealth.Emplace(BlockKey, Health - Force);
    }
    else
    {
        EditedHealth.Remove(BlockKey);
        AddBlock(BlockKey, EBlockType::Air);
    }
}

TArray<EBlockType> UPTSubsystem::GetChunkData(const FIntVector& ChunkKey)
{
    TArray<EBlockType> Data;
    FIntVector FirstBlock = ChunkKeyToBlockKey(ChunkKey);
    FIntVector LastBlock = FirstBlock + BlocksInChunk;
    Data.SetNum(BlocksInChunk.X * BlocksInChunk.Y * BlocksInChunk.Z);
    int32 i = 0;
    for (int32 z = FirstBlock.Z; z < LastBlock.Z; z++)
    {
        for (int32 y = FirstBlock.Y; y < LastBlock.Y; y++)
        {
            for (int32 x = FirstBlock.X; x < LastBlock.X; x++)
            {
                Data[i++] = NoiseAt(FIntVector(x, y, z));
            }
        }
    }
    return Data;
}

void UPTSubsystem::AddBlock(const FVector& Location, EBlockType Type)
{
    AddBlock(LocationToBlockKey(Location), Type);
}

void UPTSubsystem::AddBlock(const FIntVector& BlockKey, EBlockType Type, bool UpdateMesh)
{
    FIntVector ChunkKey = BlockKeyToChunkKey(BlockKey);

    // out of bounds
    if (BlockKey.Z == BlocksInChunk.Z || BlockKey.Z < 0)
    {
        return;
    }

    // add block, but if the block type is the procedural value, we don't need to memorize it
    EBlockType ProceduralValue = NoiseAt(BlockKey);
    if (Type == ProceduralValue)
    {
        if (UserChunks.Contains(ChunkKey) && UserChunks[ChunkKey].Contains(BlockKey))
        {
            UserChunks[ChunkKey].Remove(BlockKey);
        }
    }
    else
    {
        if (!UserChunks.Contains(ChunkKey))
        {
            UserChunks.Add(ChunkKey);
        }
        UserChunks[ChunkKey].Emplace(BlockKey, Type);
    }

    // update actor with that block
    if (TerrainChunks.Contains(ChunkKey) && UpdateMesh)
    {
        TerrainChunks[ChunkKey]->ChangeBlock(BlockKey, Type);
    }
}

void UPTSubsystem::UpdateReferencePosition()
{
    ReferencePosition = FVector();
    if (UWorld* World = GetWorld())
    {
        if (APlayerController* PlayerController = World->GetFirstPlayerController())
        {
            if (APawn* PlayerPawn = PlayerController->GetPawn())
            {
                ReferencePosition = PlayerPawn->GetActorLocation();
                ReferencePosition.Z = 0;
            }
        }
    }
}

bool UPTSubsystem::InRange(const FIntVector& ChunkKey)
{
    FVector ChunkLocation = ChunkKeyToLocation(ChunkKey) + FVector((float)BlocksInChunk.X * BlockSize / 2, (float)BlocksInChunk.Y * BlockSize / 2, 0);
    FVector Difference = ChunkLocation - ReferencePosition;
    return Difference.Size() < BlocksInChunk.X * BlockSize * DrawRadius;
}

// adds missing chunks, from the centre out
void UPTSubsystem::AddMissingChunks()
{
    FIntVector ReferenceChunkKey = LocationToChunkKey(ReferencePosition);
    bool OnePerFrameAdded = false;

    for (int32 MyDrawRadius = 0; MyDrawRadius <= DrawRadius; MyDrawRadius++)
    {
        if (OnePerFrameAdded)
        {
            break;
        }

        for (int32 X = ReferenceChunkKey.X - MyDrawRadius; X <= ReferenceChunkKey.X + MyDrawRadius; X++)
        {
            for (int32 Y = ReferenceChunkKey.Y - MyDrawRadius; Y <= ReferenceChunkKey.Y + MyDrawRadius; Y++)
            {
                if (X != ReferenceChunkKey.X - MyDrawRadius && X != ReferenceChunkKey.X + MyDrawRadius &&
                    Y != ReferenceChunkKey.Y - MyDrawRadius && Y != ReferenceChunkKey.Y + MyDrawRadius)
                {
                    continue;
                }

                FIntVector ChunkKey = FIntVector(X, Y, 0);
                if (ChunkExists(ChunkKey) || !InRange(ChunkKey))
                {
                    continue;
                }

                SpawnChunk(ChunkKey);
                OnePerFrameAdded = true;
                break;
            }
        }
    }
}

void UPTSubsystem::RemoveDistantChunks()
{
    TArray<FIntVector> ToRemove;
    for (auto It = TerrainChunks.CreateConstIterator(); It; ++It)
    {
        if (!InRange(It.Key()))
            ToRemove.Add(It.Key());
    }

    for (auto ChunkKey : ToRemove)
        DespawnChunk(ChunkKey);
}

void UPTSubsystem::SpawnChunk(FIntVector const& ChunkKey)
{
    FTransform Transform = FTransform();
    Transform.SetLocation(ChunkKeyToLocation(ChunkKey));
    AMOSTerrainChunk* SpawnedChunk = GetWorld()->SpawnActor<AMOSTerrainChunk>(AMOSTerrainChunk::StaticClass(), Transform);
    TerrainChunks.Emplace(ChunkKey, SpawnedChunk);
    FIntVector BlockKeyFrom = ChunkKeyToBlockKey(ChunkKey);
    SpawnedChunk->Init(ChunkKey, BlocksInChunk, BlockSize);
}

void UPTSubsystem::DespawnChunk(FIntVector const& ChunkKey)
{
    if (ChunkExists(ChunkKey))
    {
        TerrainChunks[ChunkKey]->Destroy();
        TerrainChunks.Remove(ChunkKey);
    }
}

EBlockType UPTSubsystem::NoiseAt(const FIntVector& BlockKey)
{
    FVector Location = FVector(BlockKey) * BlockSize;

    if (Location.Z <= BlockSize)
    {
        return EBlockType::Lava;
    }

    float Output = 0;
    //base hills
    Output += FMath::PerlinNoise2D(FVector2D(Location.X, Location.Y) * .00007f) * .5f + .5f;
    // valleys vs mountains
    Output = Output * Output * Output;
    Output += (Output + .4) * 0.4f;

    // // very random result which will be more prominent the higher we are
    Output += Output * Output * FMath::PerlinNoise2D(FVector2D(Location.X, Location.Y) * 0.003f) * .07f;

    float MaxLevel = MaxHeight * BlockSize;
    float HeightRatio = FMath::Clamp(Location.Z / MaxLevel, 0.f, 1.f);

    // terrain treshold, or the inside of the cave
    if (Output < HeightRatio || HeightRatio - .4f < Output && FMath::PerlinNoise3D(Location * 0.0007f) * .5f > .2f)
    {
        return EBlockType::Air;
    }

    // randomized terrain height type
    float OutputRandomizer = FMath::PerlinNoise2D(FVector2D(Location.X, Location.Y) * 0.0009f) * .2;
    float Treshold = FMath::Clamp(((float)HeightRatio * 4 + (float)OutputRandomizer), 0.f, 4.f);
    if (Treshold < .7)
    {
        return EBlockType::Sand;
    }
    if (Treshold < 2.1)
    {
        return  EBlockType::Ground;
    }
    if (Treshold < 3)
    {
        return  EBlockType::Rock;
    }
    return  EBlockType::Snow;
}
