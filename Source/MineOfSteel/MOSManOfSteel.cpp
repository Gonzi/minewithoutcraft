// Copyright 2020 Martin Jablonsky, Inc. All Rights Reserved.

#include "MOSManOfSteel.h"
#include "Components/InputComponent.h"
#include "MOSTerrainManipulatorComponent.h"

AMOSManOfSteel::AMOSManOfSteel()
{
    PrimaryActorTick.bCanEverTick = true;
    TerrainManipulator = CreateDefaultSubobject<UMOSTerrainManipulatorComponent>(TEXT("TerrainManipulatorComponent"));
    TerrainManipulator->DamageOutput = 100;
    TerrainManipulator->InteractionRange = 10000;
}

void AMOSManOfSteel::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
    PlayerInputComponent->BindAction("QuickSave", IE_Pressed, TerrainManipulator, &UMOSTerrainManipulatorComponent::QuickSave);
    PlayerInputComponent->BindAction("QuickLoad", IE_Released, TerrainManipulator, &UMOSTerrainManipulatorComponent::QuickLoad);
    PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AMOSManOfSteel::InteractPressed);
    PlayerInputComponent->BindAction("Interact", IE_Released, this, &AMOSManOfSteel::InteractReleased);
    PlayerInputComponent->BindAction("ChangeMode", IE_Pressed, TerrainManipulator, &UMOSTerrainManipulatorComponent::ChangeMode);
}

void AMOSManOfSteel::InteractPressed()
{
    IsInteracting = true;
}

void AMOSManOfSteel::InteractReleased()
{
    IsInteracting = false;
}

void AMOSManOfSteel::Tick(float DeltaSeconds)
{
    if (IsInteracting)
    {
        if (TerrainManipulator) {
            TerrainManipulator->Interact();
        }
        else {
            UE_LOG(LogTemp, Error, TEXT("MOSManOfSteel > Tick IsInteracting : Terrain not available"));
        }
    }
}