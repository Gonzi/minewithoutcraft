// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MOSGameModeBase.generated.h"

/**
 *
 */
UCLASS()
class MINEOFSTEEL_API AMOSGameModeBase : public AGameModeBase
{
    GENERATED_BODY()

};
