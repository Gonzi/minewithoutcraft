// Copyright 2020 Martin Jablonsky, All Rights Reserved.

#include "MOSClark.h"
#include "Engine/Engine.h"
#include "DrawDebugHelpers.h"
#include "MOSTerrainChunk.h"
#include "MOSTerrainUtils.h"
#include "PTSubsystem.h"
#include "MOSTerrainManipulatorComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/Controller.h"

AMOSClark::AMOSClark()
{
    PrimaryActorTick.bCanEverTick = true;
    TerrainManipulator = CreateDefaultSubobject<UMOSTerrainManipulatorComponent>(TEXT("TerrainManipulatorComponent"));
}

void AMOSClark::BeginPlay()
{
    Super::BeginPlay();
}

void AMOSClark::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
    PlayerInputComponent->BindAxis("MoveForward", this, &AMOSClark::MoveForward);
    PlayerInputComponent->BindAxis("MoveRight", this, &AMOSClark::MoveRight);
    PlayerInputComponent->BindAxis("LookUp", this, &AMOSClark::AddControllerPitchInput);
    PlayerInputComponent->BindAxis("LookRight", this, &AMOSClark::AddControllerYawInput);
    PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AMOSClark::StartJump);
    PlayerInputComponent->BindAction("Jump", IE_Released, this, &AMOSClark::StopJump);
    PlayerInputComponent->BindAction("QuickSave", IE_Pressed, TerrainManipulator, &UMOSTerrainManipulatorComponent::QuickSave);
    PlayerInputComponent->BindAction("QuickLoad", IE_Released, TerrainManipulator, &UMOSTerrainManipulatorComponent::QuickLoad);
    PlayerInputComponent->BindAction("Interact", IE_Pressed, TerrainManipulator, &UMOSTerrainManipulatorComponent::Interact);
    PlayerInputComponent->BindAction("ChangeMode", IE_Pressed, TerrainManipulator, &UMOSTerrainManipulatorComponent::ChangeMode);
}

void AMOSClark::MoveForward(float Value)
{
    FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
    AddMovementInput(Direction, Value * 50);
}

void AMOSClark::MoveRight(float Value)
{
    FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
    AddMovementInput(Direction, Value * 50);
}

void AMOSClark::StartJump()
{
    bPressedJump = true;
}

void AMOSClark::StopJump()
{
    bPressedJump = false;
}