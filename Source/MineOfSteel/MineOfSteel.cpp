// Copyright Epic Games, Inc. All Rights Reserved.

#include "MineOfSteel.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, MineOfSteel, "MineOfSteel");
