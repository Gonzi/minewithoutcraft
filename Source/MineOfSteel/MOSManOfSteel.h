// Copyright 2020 Martin Jablonsky, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpectatorPawn.h"
#include "MOSManOfSteel.generated.h"

/**
 *
 */
UCLASS()
class MINEOFSTEEL_API AMOSManOfSteel : public ASpectatorPawn
{
    GENERATED_BODY()
public:
    AMOSManOfSteel();

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Terrain");
    class UMOSTerrainManipulatorComponent* TerrainManipulator;

protected:
    bool IsInteracting = false;
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
    void InteractPressed();
    void InteractReleased();
    virtual void Tick(float DeltaSeconds) override;
};
