// Copyright 2020 Martin Jablonsky, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MOSClark.generated.h"

UCLASS()
class MINEOFSTEEL_API AMOSClark : public ACharacter
{
    GENERATED_BODY()

public:
    // Sets default values for this character's properties
    AMOSClark();
    UPROPERTY(BlueprintReadOnly, Category = "Terrain");
    class UMOSTerrainManipulatorComponent* TerrainManipulator;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    UFUNCTION()
        void MoveForward(float Value);

    UFUNCTION()
        void MoveRight(float Value);

    UFUNCTION()
        void StartJump();

    UFUNCTION()
        void StopJump();

public:

    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
