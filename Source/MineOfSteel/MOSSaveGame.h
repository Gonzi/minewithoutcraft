// Copyright 2020 Martin Jablonsky, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "MOSTerrainUtils.h"

#include "MOSSaveGame.generated.h"

/**
 *
 */
UCLASS()
class MINEOFSTEEL_API UMOSSaveGame : public USaveGame
{
    GENERATED_BODY()

public:
    UPROPERTY()
        TMap<FIntVector, EBlockType> TerrainData;
};
